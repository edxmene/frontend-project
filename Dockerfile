FROM node:14.19-slim as node1
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
RUN npm run build --prod 
#delete source
#stage 2
FROM nginx as watchFrontEnd
COPY --from=node1 /app/dist/watchout-client /usr/share/nginx/html