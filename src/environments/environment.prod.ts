export const environment = {
  production: true,
  baseurl: 'http://localhost:7165/api/user',
  mapboxtoken:
    'pk.eyJ1IjoiamExYXNvZnRkaWVnbyIsImEiOiJjbDBsbHFoYmEwZGpyM2pwbG0yY2JtaDVkIn0.gBFmS9qDOMv0KIqpv0xSXQ',
  supabase: {
    publicKey:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImFuaGdvanNrb2tjcHBhbWtwd2J5Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2NDY5MjQzODEsImV4cCI6MTk2MjUwMDM4MX0.lYsiwKCKMBIdNzrH2oGsa0aWGu0Y1ozNT0UVzrb2PQY',
    url: 'https://anhgojskokcppamkpwby.supabase.co',
  },
};
