import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ComponentsService {
  eventBarStatus = new BehaviorSubject<any>([]);
  constructor() {}
}
