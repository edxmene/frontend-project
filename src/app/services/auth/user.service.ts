import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  LoginDto,
  UserDto,
  ResetPasswordDTO,
  EmailDTO,
} from '@shared/interfaces/interfaces';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private _http: HttpClient) {}
  readonly BaseURI = environment.baseurl;

  register(body: UserDto): Observable<UserDto> {
    return this._http.post<UserDto>(this.BaseURI, body);
  }

  login(body: LoginDto): Observable<UserDto> {
    return this._http.post<UserDto>(this.BaseURI + '/login', body);
  }

  sendRecoveryPasswordEmail(body: EmailDTO): Observable<string> {
    return this._http.post<string>(this.BaseURI + '/send-recovery-email', body);
  }

  resetPassword(body: ResetPasswordDTO, authToken: string): Observable<string> {
    return this._http.post<string>(
      this.BaseURI + '/reset-password/' + authToken,
      body
    );
  }
}
