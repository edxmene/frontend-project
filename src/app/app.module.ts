import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthModule } from '@auth/auth.module';
import { MaterialUIModule } from '@material/materialUI.module';
import { MainPageModule } from '@main/main-page.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapViewComponent } from './module/main-page/components/map-view/map-view.component';
// import{DynamicTestModule};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MainPageModule,
    MaterialUIModule,
    BrowserAnimationsModule,
    MaterialUIModule,
    ReactiveFormsModule,
    HttpClientModule,
    AuthModule,
    MainPageModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
