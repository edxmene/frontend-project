import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Feature } from '@services/mapbox/mapbox-geocoding.service';
import { MapMouseEvent, LngLat, LngLatLike } from 'mapbox-gl';

export interface MarkerDetails{
  lngLat: LngLat,
  name: string,
  address: string,
  city: string,
  country: string,
  description: string
}
@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss'],
})
export class MapViewComponent {
  landmarks: Array<MarkerDetails> = [];
  selectedPoint: MarkerDetails | null = null; 

  selectedFeatureValue: Feature | undefined = undefined;

  @Output() selectedFeatureChange: EventEmitter<Feature | undefined> = new EventEmitter()

  @Input()
  get selectedFeature(): Feature | undefined {
    return this.selectedFeatureValue;
  }

  set selectedFeature(feature: Feature | undefined) {
    this.selectedFeatureValue = feature;
    this.selectedFeatureChange.emit(this.selectedFeatureValue);

    this.center = this.selectedFeatureValue?.center ?? [-74.07231699675322, 4.66336863727521];
  }

  center: LngLatLike = [-74.07231699675322, 4.66336863727521];

  constructor() {}

  setLandmark(value: MapMouseEvent) {
    let newMarker: MarkerDetails = {
      lngLat: value.lngLat,
      name: 'New marker',
      address: 'Av. Earth 123, Street 100',
      city: 'City',
      country: 'Country',
      description: 'Marker description'
    }
    this.landmarks.push(newMarker);
    this.selectedPoint = null;
  }

  onClick(event: MouseEvent, evt: MarkerDetails) {
    this.selectedPoint = evt;
    event.stopPropagation();
  }

  getStringSelected(value: MarkerDetails): string {
    return JSON.stringify(value);
  }
}

