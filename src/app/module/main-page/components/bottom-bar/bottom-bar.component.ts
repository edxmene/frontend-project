import {
  Component,
  Inject,
  InjectionToken,
  Input,
  OnInit,
} from '@angular/core';
import {
  MatBottomSheetRef,
  MAT_BOTTOM_SHEET_DATA,
} from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-bottom-bar',
  templateUrl: './bottom-bar.component.html',
  styleUrls: ['./bottom-bar.component.scss'],
})
export class BottomBarComponent implements OnInit {
  MAT_BOTTOM_SHEET_DATA!: InjectionToken<any>;
  sliderValue!: number;
  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public eventType: any,
    private _bottomSheetRef: MatBottomSheetRef<BottomBarComponent>
  ) {}

  ngOnInit(): void {}

  onCloseMenu() {
    this._bottomSheetRef.dismiss();
  }
}
