import { StringMap } from '@angular/compiler/src/compiler_facade_interface';
import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { ComponentsService } from '@services/component/components.service';
import { eventDialFabAnimations } from './event-fab.animations';

interface IButtons {
  icon: string;
}

@Component({
  selector: 'app-event-fab',
  templateUrl: './event-fab.component.html',
  styleUrls: ['./event-fab.component.scss'],
  animations: eventDialFabAnimations,
})
export class EventFabComponent implements OnInit, OnDestroy {
  fabButtons: IButtons[] = [
    {
      icon: 'map',
    },
    {
      icon: 'moving',
    },
    {
      icon: 'location_on',
    },
  ];
  buttons: IButtons[] = [];
  fabTogglerState = 'inactive';
  eventBarStatusSubs: any;
  @Output() clickEvent = new EventEmitter<string>();

  constructor(private componentsService: ComponentsService) {}

  ngOnInit(): void {
    this.eventBarStatusSubs = this.componentsService.eventBarStatus.subscribe(
      arrayOfIcons => {
        this.buttons = arrayOfIcons;
      }
    );
  }

  ngOnDestroy(): void {
    this.eventBarStatusSubs.unsubscribe();
  }
  showItems() {
    this.fabTogglerState = 'active';
    this.buttons = this.fabButtons;
  }

  hideItems() {
    this.fabTogglerState = 'inactive';
    this.buttons = [];
  }

  onToggleFab() {
    this.buttons.length ? this.hideItems() : this.showItems();
  }

  callMenu(icon: string) {
    if (icon === 'location_on') {
      this.clickEvent.emit('location_on');
    }
    if (icon === 'moving') {
      this.clickEvent.emit('moving');
    }
    if (icon === 'map') {
      this.clickEvent.emit('map');
    }
  }
}
