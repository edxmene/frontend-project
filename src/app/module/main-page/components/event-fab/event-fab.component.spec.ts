import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventFabComponent } from './event-fab.component';

describe('EventFabComponent', () => {
  let component: EventFabComponent;
  let fixture: ComponentFixture<EventFabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EventFabComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventFabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
