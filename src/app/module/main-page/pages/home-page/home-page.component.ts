import { Component } from '@angular/core';
import { Feature } from '@services/mapbox/mapbox-geocoding.service';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { BottomBarComponent } from '@main/components/bottom-bar/bottom-bar.component';
import { ComponentsService } from '@services/component/components.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent {
  showSideBar: boolean = false;
  eventType!: string;

  selectedFeature: Feature | undefined = undefined;

  constructor(
    private _bottomSheet: MatBottomSheet,
    private componentsService: ComponentsService
  ) {}

  onTextChange(text: string | Event) {
    console.log(text);
  }

  onPlaceSelected(feature: Feature) {}

  openBottomSheet(eventType: any) {
    this._bottomSheet.open(BottomBarComponent, {
      hasBackdrop: false,
      data: eventType,
    });
    this.componentsService.eventBarStatus.next([]);
  }
}
