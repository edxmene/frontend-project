import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserService } from '@services/auth/user.service';
import { saveLocalStorage } from '@shared/helpers/helpers';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  loading = false;
  subscription!: any;
  hide_pwd = true;
  hide_confirm_pwd = true;

  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private readonly _userService: UserService
  ) {}

  ngOnInit(): void {
    this.form = this._fb.group(
      {
        firstName: new FormControl('', [
          Validators.required,
          Validators.minLength(3),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.maxLength(20),
        ]),
        lastName: new FormControl('', [
          Validators.required,
          Validators.minLength(3),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.maxLength(20),
        ]),
        email: new FormControl('', [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
          Validators.email,
        ]),
        password: new FormControl('', [
          Validators.required,
          Validators.pattern(
            '^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[_#?!@$%^&*-]).{8,}$'
          ),
        ]),
        confirmPassword: new FormControl('', [Validators.required]),
      },
      {
        validators: this.MustMatch('password', 'confirmPassword'),
      }
    );
  }

  //Getters to Form
  get firstName() {
    return this.form.get('firstName');
  }
  get lastName() {
    return this.form.get('lastName');
  }
  get email() {
    return this.form.get('email');
  }
  get password() {
    return this.form.get('password');
  }
  get confirmPassword() {
    return this.form.get('confirmPassword');
  }

  //Validators

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors['MustMatch']) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ MustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  //Register

  signUp() {
    const user = this.form.value;
    this.subscription = this._userService.register(user).subscribe({
      next: res => {
        this.fakeLoading();
        saveLocalStorage(res.authToken);
        this._router.navigateByUrl('/home');
      },
      error: err => {
        this.error(err.error.message || 'Check fields');
      },
    });
  }

  //Helpers to Register

  fakeLoading() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 1580);
  }

  error(message: string) {
    this._snackBar.open(message, '', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }
}
