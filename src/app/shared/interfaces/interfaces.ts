export interface LoginDto {
  email: string;
  password: string;
}

export interface UserDto {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  authToken: string;
}

export interface ResetPasswordDTO {
  newPassword: string;
}

export interface EmailDTO {
  email: string;
}
